Pod::Spec.new do |s|
  s.name = 'PureLiveSDK'
  s.version = '8.5.0'
  s.summary = 'PureLiveSDK'
  s.homepage = 'https://gitlab.com/purelive/purelive-ios-sdk'
  s.authors = { 'PureLive' => 'info@purelive.ae' }
  s.source = { :git => 'https://gitlab.com/purelive/purelive-ios-sdk' }
  s.ios.deployment_target  = '11.0'
  s.default_subspec = 'Full'

  s.subspec 'Full' do |ss|
    ss.vendored_frameworks = 'PureLiveSDK.xcframework'
    ss.resources = ['PureLiveSDKResources.bundle', 'PureLiveSDKOnDeviceResources.bundle']
  end
  
  s.subspec 'Core' do |ss|
    ss.vendored_frameworks = 'PureLiveSDK.xcframework'
    ss.resources = 'PureLiveSDKResources.bundle'
  end
  
  s.dependency 'TensorFlowLiteC', '2.11.0'
  
end
