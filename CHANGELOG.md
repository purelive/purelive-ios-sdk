## [2.3.0]

- Added methods of on-device analysis: runOnDeviceLivenessAnalysis and runOnDeviceBiometryAnalysis. 
- You can choose the installation version. Standard installation gives access to full functionality. The core version (OzLivenessSDK/Core) installs SDK without the on-device functionality.
- Added a method to upload data to server and start analyzing it immediately: uploadAndAnalyse.
- Improved the licensing process, now you can add a license when initializing SDK: OZSDK(licenseSources: [LicenseSource], completion: @escaping ((LicenseData?, LicenseError?) -> Void)), whereLicenseSource is a path to physical location of your license, LicenseData contains the license information.
- Added the setLicense method to force license adding.

## [2.2.4]

- Added a function to initialize an SDK with license sources.

- For initialization you should call an init method from PureLiveSDK with a list of LicenseSources and completion handler: (LicenseData?, LicenseError?) -> Void:

```swift
PureLiveSDK(licenseSources: [LicenseSource.licenseFilePath("absolute_path_to_your_license_file")], completion: @escaping ((LicenseData?, LicenseError?) -> Void))
```
- Added a var with license data in runtime
```swift
let licenseData = PureLiveSDK.licenseData
```
